package org.eclipse.tracecompass.incubator.internal.psf.analyser.core.analysis;

import java.util.Objects;

import org.eclipse.tracecompass.statesystem.core.ITmfStateSystemBuilder;
import org.eclipse.tracecompass.statesystem.core.exceptions.StateValueTypeException;
import org.eclipse.tracecompass.statesystem.core.exceptions.TimeRangeException;
import org.eclipse.tracecompass.tmf.core.event.ITmfEvent;
import org.eclipse.tracecompass.tmf.core.event.TmfEvent;
import org.eclipse.tracecompass.tmf.core.statesystem.AbstractTmfStateProvider;
import org.eclipse.tracecompass.tmf.core.statesystem.ITmfStateProvider;
import org.eclipse.tracecompass.tmf.core.trace.ITmfTrace;
import org.eclipse.tracecompass.tmf.ctf.core.event.CtfTmfEvent;

public class PsfStateProvider extends AbstractTmfStateProvider {

    public PsfStateProvider(ITmfTrace trace) {
        super(trace, "psf");
        // TODO Auto-generated constructor stub
    }

    @Override
    public int getVersion() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public ITmfStateProvider getNewInstance() {
        // TODO Auto-generated method stub
        return new PsfStateProvider(getTrace());
    }

    @Override
    protected void eventHandle(ITmfEvent ev) {
        // TODO Auto-generated method stub


        TmfEvent event = (TmfEvent) ev;

        final long ts = event.getTimestamp().getValue();
        //Integer nextTid = ((Long) event.getContent().getField("next_tid").getValue()).intValue();
        String container_targ = ((String) event.getContent().getField("container_targ").getValue());

        try {

            if (event.getType().getName().equals("lttng_tracing:zmq_zsock_bind_event")) {
                ITmfStateSystemBuilder ss = Objects.requireNonNull(getStateSystemBuilder());
                int quark = ss.getQuarkAbsoluteAndAdd(container_targ);
                String socket_id = ((String) event.getContent().getField("new_bind_adress").getValue());
                String event_label;
                String event_label_type = "Bind socket ";
                String event_label_value = socket_id;
                event_label = event_label_type + ": " + event_label_value;
                ss.modifyAttribute(ts, event_label, quark);

            }
            else if (event.getType().getName().equals("lttng_tracing:zmq_zmsg_new")) {
                ITmfStateSystemBuilder ss = Objects.requireNonNull(getStateSystemBuilder());
                int quark = ss.getQuarkAbsoluteAndAdd(container_targ);
                String message_id = ((String) event.getContent().getField("message_id").getValue());

                String event_label;
                String event_label_type = "New message";
                String event_label_value = message_id;
                event_label = event_label_type + ": " + event_label_value;
                ss.modifyAttribute(ts, event_label, quark);
            }
            else if (event.getType().getName().equals("lttng_tracing:zmq_zframe_new")) {
                ITmfStateSystemBuilder ss = Objects.requireNonNull(getStateSystemBuilder());
                String message_id = ((String) event.getContent().getField("message_id").getValue());

                String message_content = ((String) event.getContent().getField("content").getValue());
                int quark = ss.getQuarkAbsoluteAndAdd(container_targ);
                String event_label;
                String event_label_type = "New Frame in message";
                String event_label_value = message_id;
                String event_label_content = message_content;
                event_label = event_label_type + ":" + event_label_value +", " + event_label_content;
                ss.modifyAttribute(ts, event_label, quark);
            }

            else if (event.getType().getName().equals("lttng_tracing:zmq_zmsg_destroy")) {
                ITmfStateSystemBuilder ss = Objects.requireNonNull(getStateSystemBuilder());
                int quark = ss.getQuarkAbsoluteAndAdd(container_targ);
                String message_id = ((String) event.getContent().getField("message_id").getValue());

                String event_label;
                String event_label_type = "Destroy message";
                String event_label_value =message_id;
                event_label = event_label_type + ": " + event_label_value;
                ss.modifyAttribute(ts, event_label, quark);

            }
            else if (event.getType().getName().equals("lttng_tracing:zmq_zmsg_destroy_end")) {
                ITmfStateSystemBuilder ss = Objects.requireNonNull(getStateSystemBuilder());
                int quark = ss.getQuarkAbsoluteAndAdd(container_targ);
                String message_id = ((String) event.getContent().getField("message_id").getValue());
                ss.removeAttribute(ts, quark);
            }

            else if (event.getType().getName().equals("lttng_tracing:zmq_zmsg_recv")) {
                ITmfStateSystemBuilder ss = Objects.requireNonNull(getStateSystemBuilder());
                int quark = ss.getQuarkAbsoluteAndAdd(container_targ);
                String message_id = ((String) event.getContent().getField("message_id").getValue());
                String message_content = ((String) event.getContent().getField("content").getValue());
                String container_orig = ((String) event.getContent().getField("content").getValue());
                String event_label;
                String event_label_type = "Receive message";
                String event_label_value =message_id;
                String event_label_type2 = " From ";
                event_label = event_label_type + ": " + event_label_value + event_label_type2 + container_orig + "," + message_content;
                ss.modifyAttribute(ts, event_label, quark);
            }






        } catch (TimeRangeException e) {
            /*
             * This should not happen, since the timestamp comes from a trace
             * event.
             */
            throw new IllegalStateException(e);
        } catch (StateValueTypeException e) {
            /*
             * This wouldn't happen here, but could potentially happen if we try
             * to insert mismatching state value types in the same attribute.
             */
            e.printStackTrace();
        }
    }

}

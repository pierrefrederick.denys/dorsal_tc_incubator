package org.eclipse.tracecompass.incubator.internal.psf.analyser.core.analysis;

import org.eclipse.tracecompass.tmf.core.statesystem.ITmfStateProvider;
import org.eclipse.tracecompass.tmf.core.statesystem.TmfStateSystemAnalysisModule;

/**
 * @author pierre
 *
 */
public class PsfAnalysisModule extends TmfStateSystemAnalysisModule {

    @Override
    protected ITmfStateProvider createStateProvider() {
        // TODO Auto-generated method stub
        return new PsfStateProvider(getTrace());
    }




}
